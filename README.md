# PlantUML-Sequence-MovieTickets

You are designing a system for an online movie ticket booking platform. The system should allow users to search for movies, view movie details, select seats, and make payments. The system should also allow administrators to add and remove movies and manage movie showtimes.

Create a sequence diagram using PlantUML that illustrates the following scenario:

    A user searches for a movie by title.
    The system returns a list of matching movies.
    The user selects a movie and views its details, including the available showtimes.
    The user selects a showtime and chooses seats.
    The system confirms the seats are available and creates a reservation.
    The user proceeds to checkout and completes the payment.

Make sure to include all the necessary actors, objects, and methods in your diagram. You can assume that the user is already authenticated and has a valid session token.

```plantuml
@startuml MovieTickets
actor User
boundary "User Interface" as UserInterface
database "Movie Database" as MovieDatabase

database "Showtime Database" as ShowTimeDatabase

database "Seat Database" as SeatDatabase

database "Payment Database" as PaymentDatabase

autonumber 
User -> UserInterface ++: Search Movie by Title
    UserInterface -> MovieDatabase ++: GET /api/v1/movie:Title
    MovieDatabase --> UserInterface --: HTTP 200
UserInterface --> User --: Display Movies


User -> UserInterface ++: Select Movie
    UserInterface -> MovieDatabase ++: GET /api/v1/movie:id
    MovieDatabase --> UserInterface --: HTTP 200
    UserInterface -> ShowTimeDatabase ++: GET /api/v1/showTime/movie:id
    ShowTimeDatabase --> UserInterface --: HTTP 200
UserInterface --> User --: Display Movie Details


User -> UserInterface ++: Select Show Time\nand Choose a seat
    UserInterface -> SeatDatabase ++: GET /api/v1/seat/showTime:id&&seatLocation:id

    autonumber 13 "<font color=green>"
    alt "Seat is available"
        SeatDatabase [#Green]--> UserInterface : HTTP 200
        UserInterface [#Green]-> SeatDatabase : POST /api/v1/seat/new/seat:id
        SeatDatabase [#Green]--> UserInterface : HTTP 201
        UserInterface [#Green]--> User : Seat Reserved
    autonumber 13 "<font color=red>"
    else "Seat is not available"
        SeatDatabase [#Red]--> UserInterface --: HTTP 406 
        UserInterface [#Red]-> User -- : Seat is not available
    end
    

autonumber 17
User -> UserInterface ++: Proceed to Checkout
    UserInterface -> PaymentDatabase ++: POST /api/v1/payment/new/seat:id
    PaymentDatabase --> UserInterface --: HTTP 201
UserInterface --> User --: Payment Completed

@enduml
```
